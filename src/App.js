import React from 'react';
import firebase from 'firebase/app';
import 'firebase/auth'
import firebaseConfig from './config/firebaseConfig';
import MainRoutes from './routes/MainRoutes';

import userContext from './context';
import UsersController from './controllers/UsersController';

class App extends React.Component {
	static contextType = userContext;

	constructor() {
		super();
		if (!firebase.apps.length) {
			firebase.initializeApp(firebaseConfig);
		}
	}

	componentDidMount() {
		firebase.auth().onAuthStateChanged((user) => {
			if (!this.context.user && user) {
				UsersController.create({
					full_name: user.displayName,
					email: user.email,
					username: user.email.split('@')[0],
					photo_url: user.photoURL
				}).then((res) => {
					this.context.login({
						...user,
						...res.data
					});
					// this.props.history.push('/dashboard/');
				});
			}
		});
	}

	render() {
		return <MainRoutes />;
	}
}

export default App;
