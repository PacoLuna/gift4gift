import React, { Component } from 'react'
import {Avatar, Box, Button, Flex, Heading,Text,Stack, Textarea} from '@chakra-ui/core'
import CommentsController from '../../controllers/CommentsController'
import userContext from '../../context'
import {withRouter} from 'react-router-dom'

class CommentsBox extends Component {

    static contextType = userContext;

    state = {
        message: ""
    }

    createComment = ()=>{
        const { message } = this.state;
        const { id } = this.props.match.params;
        const user_id = this.context.user.id;

        if(message){
            CommentsController.create(id, {
                message, 
                user_id
            }).then(()=>{
                this.setState({message: ''});
                this.props.getGift(id);
            })
        }
    }
    setMessage =(e)=>this.setState({message: e.target.value})
    render() {
        const {comments} = this.props;
        return (
            <Flex align="center" justify="center" flexDirection="column" w="100%">
                
                <Flex marginBottom="1rem" marginTop="2%">
                    <Heading as="h3" size="lg">Comentarios ({comments.length})</Heading>
                    <hr/>
                </Flex>

                <Box p={3} bg="blue.50" marginTop="2%" padding="4rem">
                    <Box p={3}>
                        {
                            comments.map(element => <React.Fragment key={element.id}>
                                <Stack isInline marginTop="1rem" >
                                    <Avatar name={element.user.full_name} src={element.user.photo_url} size="sm"/>
                                    <Text>{element.user.username}</Text>
                                </Stack>
                                <Text bg="white" marginY="0.5rem" padding="1.5rem" textAlign="justify">{element.message}</Text>
                            </React.Fragment>)
                        }
                    </Box>
                    <Textarea w="400px" h="150px" marginBottom="0.5rem" placeholder="Dejános tu opinión" onChange={this.setMessage} value={this.state.message}/>
                    <Button variantColor="green" variant="solid"size="sm" marginTop="1%" onClick={this.createComment}>Compartir Comentario</Button>
                </Box>
            </Flex>
        )
    }
}

export default withRouter(CommentsBox);
