import React, { Component } from 'react'
import {Box, Button, Flex, Heading, Image, Link, Text} from '@chakra-ui/core';
import {MdStar} from 'react-icons/md'
import {withRouter} from 'react-router-dom'
import OfferModal from './OfferModal';

import userContext from '../../context'
import UsersController from '../../controllers/UsersController';

class GiftsDetails extends Component {
    static contextType = userContext;

    state = {
        myOffers: []
    }

    componentDidMount() {
        console.log(this.context)
        UsersController.find(this.context.user.id).then(res => {
            const myOffers = res.data.gifts//.filter((el) => !el.is_offer )
            this.setState({ myOffers })
        })
    }
    

    render() {
        const { gift } = this.props;
        console.log(gift);
        

        return (

            <Flex align="center" justify="center" w="100%">
                <Flex  w="100%" flexDirection="column" >
                    <Flex align="center" justify ="center" marginTop="4%">
                        <Image src={gift.photo_url}  style = {{width:735, height:350, objectFit:"contain"}} />
                    </Flex>
                    <Flex flexDirection="row" marginTop="2%" marginLeft ="10%" marginRight ="10%">
                        <Flex alignSelf="flex-start" size= "70%">
                            <Heading size="2xl" >{gift.name}</Heading>
                        </Flex>
                        <Flex alignSelf="flex-end" size= "30%" justify="flex-end" align="center">
                            <Text fontSize="md" fontWeight="bold">La oferta termina: {gift.date_limit}</Text>
                        </Flex>
                    </Flex>
                    <Flex marginLeft="13%" marginRight="13%" flexDirection="row" marginTop = "1%">
                        <Text fontSize="lg">
                            Producto ofrecido por: 
                            <Link onClick={()=> this.props.history.push(`/user/${gift.user && gift.user.id}`)}>
                                {gift.user && gift.user.full_name}
                            </Link>
                        </Text>
                        <Flex flexDirection="row" align="center" marginLeft="2%">        
                            <Box as={MdStar} color="yellow.500" />
                            <Box as={MdStar} color="yellow.500" />
                            <Box as={MdStar} color="yellow.500" />
                            <Box as={MdStar} color="black.500" />
                            <Box as={MdStar} color="black.500" />
                        </Flex>
                    </Flex>
                    <Heading as="h3" size="lg" marginBottom="1%" marginLeft="10%" marginTop="2%">
                        Descripción del producto
                    </Heading>    
                    <Text fontSize="md"  marginLeft="13%" marginRight="13%" marginBottom="2%">{gift.description}</Text>
                    <Box marginLeft="10%">
                        <OfferModal myOffers={this.state.myOffers} />
                    </Box>
                </Flex>
            </Flex>
        )
    }
}

export default withRouter(GiftsDetails)