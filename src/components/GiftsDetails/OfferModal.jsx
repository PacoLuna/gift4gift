import React, { useState } from 'react'
import{
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    useDisclosure,
    Button,
    Heading,
    Stack,
    Link,
    Text,
    useToast
} from '@chakra-ui/core'
import { useParams, useHistory } from 'react-router-dom';
import GiftController from '../../controllers/GiftsController';

export default function OfferModal({ myOffers }) {
    const toast = useToast();
    const { isOpen, onOpen, onClose } = useDisclosure();
    const params = useParams();
    const history = useHistory()
    const [ offer, setOffer ] = useState(0);

    const submitOffer = () => {
        GiftController.create_offer(params.id, offer)
        toast({
          title: "Oferta realizada correctamente",
          status: "success",
          duration: 4000,
          isClosable: true,
        })
        onClose()
    }

    return (
        <>
        <Button variantColor="green" size="lg" justify="center" onClick={onOpen}>Hacer oferta de intercambio</Button>

        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
            <ModalHeader><Heading as="h3" size="lg">Hacer oferta de intercambio</Heading></ModalHeader>
            <ModalCloseButton />
            <ModalBody>
                <Stack>
                    <Heading as="h5" size="sm">¿Quieres usar algún artículo que tengas almacenado?</Heading>
                    <select onChange={(e) => setOffer(e.target.value)}>
                        <option key="0" value={0}>Seleccionar de tus opciones</option>
                        {
                            myOffers.map((offer) => !offer.gift_id && (
                                <option key={offer.id} value={offer.id}>{offer.name}</option>
                            ))
                        }
                    </select>

                    <Text>
                        ¿No aparece tu producto en la lista? 
                        <Link 
                            style={{ color: "blue", marginLeft: 3, fontWeight: "bold"}}
                            onClick={()=>{ history.push("/gifts/new")}}
                        >
                            Agrégalo
                        </Link>
                    </Text>
                </Stack>
            </ModalBody>

            <ModalFooter>
                <Button variantColor="green" mr={3} onClick={submitOffer}>
                    Subir oferta
                </Button>
                <Button variant="ghost" onClick={onClose}>Cancelar</Button>
            </ModalFooter>
            </ModalContent>
        </Modal>
        </>
    );

}
