import React from 'react'
import {Heading, Flex, Text} from '@chakra-ui/core';
import '../../scss/Jumbotron.scss';
import SingUpForm from '../SignUpForm';

export default function Jumbotron() {
    return (
        <div className="jumbotron-background">
            <div className="jumbotron-content">
                <Flex align="center">
                    <Heading as="h2" size="xl">Gift4Gift</Heading>
                </Flex>
                <Flex align="center">
                    <Heading as="h4" size="md">La plataforma de intercambios donde cambias lo que tienes por lo que quieres</Heading>
                </Flex>
                <Flex align="center">
                    <Text>
                        Únete a la comunidad de Gift4Gift y encuentra los artículos que has estado buscando sin tener que gastar ni un sólo centavo.
                        Simplemente registrate, sube un producto de valor similar para cambiar con alguien y cambia lo que tienes, por lo que quieres
                    </Text>
                </Flex>
            </div>

            <SingUpForm/>
        </div>
    )
}
