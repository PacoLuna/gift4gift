import React from 'react';
import { Badge, Box, Flex, Image, Text, Link } from '@chakra-ui/core';
import { MdStar } from 'react-icons/md';
import { useHistory } from 'react-router-dom';
import '../../scss/Dashboard.scss';

export default function DashboardItem({ gift, category, profile, specifigPath, noPhoto }) {
	const history = useHistory();
	const stars = Math.floor(Math.random() * 5);

	const goldStars = [];
	const blackStars = [];

	for (let i = 0; i < 5 ; i++) {
		if(i < stars) goldStars.push(<Box key={i} as={MdStar} color="yellow.500" />)
		else blackStars.push(<Box key={i} as={MdStar} color="black.500" />)
	}

	return (
		<Box w="300px" borderWidth="1px" padding="1.5rem">
			{!noPhoto ? <Image rounded="md" src={gift.photo_url} style = {{width:375, height:200, objectFit:"contain"}} /> : null }
			<Flex align="baseline" mt={2}>
				<Badge variantColor="green">{category ? category : gift.category ? gift.category.name : ""}</Badge>
			</Flex>
			<Link 
				onClick={() => 
					history.push(`/${specifigPath ? specifigPath  : "gifts"}/${gift.id}`)
				}
			>
				<Text mt={2} fontSize="xl" fontWeight="semibold" lineHeight="short">
					{gift.name}
				</Text>
			</Link>
			{!profile && (<Text>
				Ofertado por: 
				<Link 
					onClick={() => history.push(`/user/${gift.user.id}`)}
				>
					{gift.user.full_name}
				</Link>
			</Text>)}

			<Flex align="center">
				{goldStars}{blackStars}
			</Flex>

			<Link onClick={() => history.push(`/gifts/${gift.id}`)}>Mas información</Link>
		</Box>
	);
}
