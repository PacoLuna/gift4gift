import React, { Component } from 'react'
import { Box, Heading, Stack} from '@chakra-ui/core';
import { useHistory } from 'react-router-dom';
import './Sidebar.scss'

import CategoriesController from '../../controllers/CategoriesController'

const Category = ({ name, id }) => {
  const history = useHistory();

  return (
    <Box 
      p={2} 
      shadow="xs" 
      borderWidth="1px" 
      as="button" 
      className="category-box" 
      onClick={() => history.push(`/categories/${id}`)}
    >
      <Heading as="h6" size="xs">{name}</Heading>
    </Box>
  );
}
export default class Sidebar extends Component {
  state = {
      categories: []
  }

  componentDidMount() {
    CategoriesController.all().then((data) => {
      this.setState({categories: data.data})
    })
  }
  
  render() {
    const { categories } = this.state;

    return (
      <div className="sidebar-content">
        <Heading marginBottom="1rem">Categorías</Heading>
        <Stack spacing={8}>
          {categories.map(({name, id})=>( <Category key={name} name={name} id={id}/> ))}
        </Stack>
      </div>
    )
  }
}
