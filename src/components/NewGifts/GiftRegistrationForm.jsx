import React from 'react';
import {
	FormControl,
	FormLabel,
	FormHelperText,
	Input,
	Textarea,
	Heading,
	Button,
	Flex,
	RadioGroup,
    Radio,
    Progress
} from '@chakra-ui/core';
import firebase from 'firebase/app';
import 'firebase/storage'
import { withRouter } from 'react-router-dom';

import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import GiftController from '../../controllers/GiftsController';
import CategoriesController from '../../controllers/CategoriesController';

import userContext from '../../context';

class GiftRegistrationForm extends React.Component {
	static contextType = userContext;

	state = {
		photo_url: null,
		photo_file: null,
		name: null,
		description: null,
		date_limit: new Date(),
		category_id: 1,
		categories: [],
		progress: 0
	};

	componentDidMount() {
		CategoriesController.all().then((data) => this.setState({ categories: data.data }));
	}

	handleChange = (date) => {
		this.setState({
			date_limit: date
		});
	};

	setName = (e) => this.setState({ name: e.target.value });
	setDescription = (e) => this.setState({ description: e.target.value });
	setCategoryId = (e) => this.setState({ category_id: e.target.value });
	setImage = (e) => this.setState({ photo_file: e.target.files[0] });

	createGift = (photo_url) => {
		const { name, description, date_limit, category_id } = this.state;
		const { user } = this.context;

		if (name === null || description === null || date_limit === null || photo_url === null) {
		} else {
			GiftController.create({
					photo_url,
					name,
					description,
					date_limit,
					category_id,
					user_id: user.id
			}).then((res) => {
				this.props.history.push(`/gifts/${res.data.id}`)
			});
		}
	};

	uploadGift = () => {
		if(!this.state.photo_file) {
			this.createGift("https://image.flaticon.com/icons/svg/1493/1493693.svg")
			return
		}
		const storage = firebase.storage().ref().child(this.state.photo_file.name);
		const task = storage.put(this.state.photo_file);

		task.on(
			'state_changed',
			(snapshot) => {
				let progress = snapshot.bytesTransferred / snapshot.totalBytes * 100;
				this.setState({ progress: progress });
			},
			(error) => {
				console.log(error.message);
			},
			() => {
                task.snapshot.ref.getDownloadURL().then((downloadURL) => {
                    this.createGift(downloadURL)
                });
			}
		);
	};

	render() {
		return (
			<Flex align="center" justify="center">
				<FormControl isRequired w="90%" h="70%" margin="3rem">
					<Heading as="h2" size="xl" margin="0.5rem">
						Registrar producto
					</Heading>
					<hr />
					<FormLabel margin="0.5rem">Seleccione foto del archivo</FormLabel>

					<br />
					<Input 
						margin="0.5rem" 
						type="file" 
						id="image" 
						accept="image/png, image/jpeg, image/svg" 
						onChange={this.setImage}
					/>
					<Progress hasStripe isAnimated value={this.state.progress}/>
					
					<br />

					<FormLabel htmlFor="productName" margin="0.5rem">
						Nombre del producto
					</FormLabel>
					<Input id="productName" placeholder="Nombre del producto" margin="0.5rem" onChange={this.setName} />

					<FormLabel htmlFor="description" margin="0.5rem">
						Descripción del producto
					</FormLabel>
					<Textarea
						id="description"
						h="200px"
						placeholder="Trata de describir lo más posible las características del producto (modelo, proveedor, marca, número de serie, etc)"
						margin="0.5rem"
						onChange={this.setDescription}
					/>

					<FormLabel margin="0.5rem">Categoría del producto</FormLabel>
					<FormControl as="fieldset" bg="#ffffff" padding="1.5rem">
						<RadioGroup onChange={this.setCategoryId} value={this.state.category_id}>
							{this.state.categories.map((element) => (
								<Radio
									value={`${element.id}`}
									key={element.name}
									isChecked={element.id === this.state.category_id}
								>
									{element.name}
								</Radio>
							))}
						</RadioGroup>

						<FormHelperText id="email-helper-text">
							Seleccione la categoría que más se aproxime a tu producto
						</FormHelperText>
					</FormControl>

					<br />
					<FormLabel margin="0.5rem">Fecha de cierre de oferta</FormLabel>
					<br />
					<DatePicker selected={this.state.date_limit} onChange={this.handleChange} />

					<Flex alignItems="center" justify="flex-end" marginTop="2rem">
						<Button variantColor="green" size="md" marginRight="0.5rem" onClick={this.uploadGift}>
							Agregar
						</Button>
							<Button 
									variantColor="red"
									size="md" 
									variant="outline" 
									marginLeft="0.5rem" 
									onClick={()=>this.props.history.push("/")}
							>
							Cancelar
						</Button>
					</Flex>
				</FormControl>
			</Flex>
		);
	}
}

export default withRouter(GiftRegistrationForm)