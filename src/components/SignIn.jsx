import React from 'react';
import { FormControl, FormLabel, Input, Image, Button, Flex } from '@chakra-ui/core';
import gift from '../GenericAssets/giftbox.svg';
import firebase from 'firebase/app';
import 'firebase/auth' 
import { withRouter } from 'react-router-dom';
import userContext from '../context';
import UsersController from '../controllers/UsersController';

class SignIn extends React.Component {
	static contextType = userContext;

	state = {
		email: null,
		password: null
	};

	loginWithGoogle = () => {
		const provider = new firebase.auth.GoogleAuthProvider();
		firebase
			.auth()
			.signInWithPopup(provider)
			.then((result) => {
				const user = result.user;

				UsersController.create({
					full_name: user.displayName,
					email: user.email,
					username: user.email.split('@')[0],
					photo_url: user.photoURL
				}).then((res) => {
					this.context.login({
						...user,
						...res.data
					});
					this.props.history.push('/dashboard/');
				});
			})
			.catch(function(error) {
				alert('Un error ocurrio al iniciar sesion');
			});
	};
	setEmail = (e) => this.setState({ email: e.target.value });
	setPassword = (e) => this.setState({ password: e.target.value });

	SignInWithEmail = () => {
		const { email, password } = this.state;
		const { history } = this.props;
		firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then((result) => {
				history.push('/dashboard/');
				const user = result.user;

				UsersController.create({
					full_name: user.displayName,
					email: user.email,
					username: user.email.split('@')[0],
					photo_url: user.photoURL
				}).then((res) => {
					this.context.login({
						...user,
						...res.data
					});
					this.props.history.push('/dashboard/');
				});
			})
			.catch(function(error) {
				alert("Un error ocurrio al ")
			});
	};

	render() {
		return (
			<Flex justifyContent="center" alignItems="center" bg="#010038" height="100vh">
				<Flex direction="column" align="center" justify="center" bg="blue.50" width="35%">
					<Image src={gift} size="50px" marginY="2%" marginTop="10%" />
					<FormControl align="center" justify="center" flexDirection="column">
						<FormLabel htmlFor="fname" style={{ marginTop: '5%' }}>
							Correo electrónico
						</FormLabel>
						<Input placeholder="Correo electrónico" style={{ width: 400 }} onChange={this.setEmail} />
						<FormLabel htmlFor="fname" style={{ marginTop: '5%' }}>
							Contraseña
						</FormLabel>
						<Input placeholder="Contraseña" style={{ width: 400 }} onChange={this.setPassword} />
						<Button
							style={{
								width: 400,
								marginTop: '5%',
								marginBottom: '2%',
								width: 400,
								height: 48,
								size: 'lg'
							}}
							variant="solid"
							variantColor="green"
							onClick={this.SignInWithEmail}
						>
							Iniciar sesión
						</Button>
						<br />
						<Button
							style={{ width: 400, marginBottom: '10%', width: 400, height: 48, size: 'lg' }}
							variant="solid"
							variantColor="green"
							onClick={this.loginWithGoogle}
						>
							Iniciar sesión con Google
						</Button>
					</FormControl>
				</Flex>
			</Flex>
		);
	}
}

export default withRouter(SignIn);
