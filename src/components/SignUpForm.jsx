import React from 'react';
import { FormControl, FormLabel, Input, Flex, Button, Text } from '@chakra-ui/core';
import firebase from 'firebase/app';
import 'firebase/auth'
import { withRouter } from 'react-router-dom';

import UsersController from '../controllers/UsersController';
import userContext from '../context';

class SignUpForm extends React.Component {
    static contextType = userContext

	state = {
		name: null,
		lastnameP: null,
		lastnameM: null,
		user: null,
		email: null,
		password: null,
		confirm: null
	};

	setName = (e) => this.setState({ name: e.target.value });
	setLastNameP = (e) => this.setState({ lastnameP: e.target.value });
	setLastNameM = (e) => this.setState({ lastnameM: e.target.value });
	setUser = (e) => this.setState({ user: e.target.value });
	setEmail = (e) => this.setState({ email: e.target.value });
	setPassword = (e) => this.setState({ password: e.target.value });
	setConfirmar = (e) => this.setState({ confirm: e.target.value });

	loginWithGoogle = () => {
		const provider = new firebase.auth.GoogleAuthProvider();
		firebase
			.auth()
			.signInWithPopup(provider)
			.then((result) => {
				const user = result.user;

				UsersController.create({
					full_name: user.displayName,
					email: user.email,
					username: user.email.split('@')[0],
					photo_url: user.photoURL
				}).then((res) => {
					this.context.login({
						...user,
						...res.data
					});
					this.props.history.push('/dashboard/');
				});
			})
			.catch(function(error) {
				console.log(error);
				
				alert('Un error ocurrio al iniciar sesion');
			});
	};

	SignUpWithEmail = () => {
		const { email, password } = this.state;
		const { history } = this.props;
		firebase
			.auth()
			.signInWithEmailAndPassword(email, password)
			.then((result) => {
				history.push('/dashboard/');
				const user = result.user;

				UsersController.create({
					full_name: user.displayName,
					email: user.email,
					username: user.email.split('@')[0],
					photo_url: user.photoURL
				}).then((res) => {
					this.context.login({
						...user,
						...res.data
					});
					this.props.history.push('/dashboard/');
				});
			})
			.catch(function(error) {
				console.log(error);
				
				alert("Un error ocurrio al registrarse")
			});
	};

	//Push to firebase

	render() {
		return (
			<Flex bg={!this.props.bg && 'blue.50'} width="50%" justify="center">
				<FormControl isRequired>
					<Text
						color="black"
						fontSize="2xl"
						fontWeight="bold"
						marginBottom="5%"
						marginTop="5%"
						htmlFor="fname"
						textAlign="center"
					>
						Regístrate en Gift4Gift
					</Text>
					<FormLabel color="black" htmlFor="fname" style={{ marginTop: '3%' }}>
						Nombre
					</FormLabel>
					<Input
						height="35px"
						color="black"
						placeholder="Nombre"
						style={{ width: 400 }}
						onChange={this.setName}
					/>
					<FormLabel color="black" htmlFor="fname" style={{ marginTop: '3%' }}>
						Apellido paterno
					</FormLabel>
					<Input
						height="35px"
						color="black"
						placeholder="Apellido paterno"
						style={{ width: 400 }}
						onChange={this.setLastNameP}
					/>
					<FormLabel color="black" htmlFor="fname" style={{ marginTop: '3%' }}>
						Apellido materno
					</FormLabel>
					<Input
						height="35px"
						color="black"
						placeholder="Apellido materno"
						style={{ width: 400 }}
						onChange={this.setLastNameM}
					/>
					<FormLabel color="black" htmlFor="fname" style={{ marginTop: '3%' }}>
						Usuario
					</FormLabel>
					<Input
						height="35px"
						color="black"
						placeholder="Nombre de usuario"
						style={{ width: 400 }}
						onChange={this.setUser}
					/>
					<FormLabel color="black" htmlFor="fname" style={{ marginTop: '3%' }}>
						Correo electrónico
					</FormLabel>
					<Input
						type="email"
						height="35px"
						color="black"
						placeholder="Correo electrónico"
						style={{ width: 400 }}
						onChange={this.setEmail}
					/>
					<FormLabel color="black" htmlFor="fname" style={{ marginTop: '3%' }}>
						Contraseña
					</FormLabel>
					<Input
						type="password"
						height="35px"
						color="black"
						placeholder="Contraseña"
						style={{ width: 400 }}
						onChange={this.setPassword}
					/>
					<FormLabel color="black" htmlFor="fname" style={{ marginTop: '3%' }}>
						Confirmar contraseña
					</FormLabel>
					<Input
						type="password"
						height="35px"
						color="black"
						placeholder="Confirmar contraseña"
						style={{ width: 400 }}
						onChange={this.setConfirmar}
					/>
					<Button
						bg="#010038"
						variantColor="green"
						style={{ width: '100%', marginTop: '5%', height: 35, size: 'lg' }}
						onClick={this.SignUpWithEmail}
					>
						Regístrate
					</Button>
					<Button
						bg="#010038"
						variantColor="green"
						style={{ width: '100%', marginTop: '1%', marginBottom: '5%', height: 35, size: 'lg' }}
						onClick={this.loginWithGoogle}
					>
						Regístrate con Google
					</Button>
				</FormControl>
			</Flex>
		);
	}
}

export default withRouter(SignUpForm);
