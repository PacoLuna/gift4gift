import React, { Component } from 'react'
import {MdAccountCircle, MdExitToApp, MdDone, MdHelpOutline, MdRedeem} from 'react-icons/md'
import {Box, Button,Image, IconButton, Menu, MenuButton, MenuList, MenuItem}  from '@chakra-ui/core';
import { withRouter } from 'react-router-dom'
import Searchbar from '../Topbar/Searchbar'
import gift from '../../GenericAssets/giftbox.svg';
import intercambio from '../../GenericAssets/hacer_intercambio.svg';
import notification from '../../GenericAssets/notification.svg';
import firebase from 'firebase/app';
import 'firebase/auth'
import '../../scss/LoginNavbar.scss';
import userContext from '../../context';

class Navbar extends Component {
    static contextType = userContext;

    logout = () => {
        firebase.auth().signOut().then((res) => {
            this.context.login(null)
            this.props.history.push("/")
        })
    }

    goToPage = (page) => {
        this.props.history.push(page)
    }

    render() {
        return (
            <div>
                <nav className="login-navbar">
                    <Button size='md' className="login-home" variant="ghost" onClick={() => this.goToPage("/")}>
                        <Image size="30px" src={gift}/>
                    </Button>
                    <div className="login-input">
                        <Searchbar/>
                    </div>

                    <div className="login-options">
                        <Button size='md' className="login-items" onClick={() => this.goToPage("/gifts/new")}>
                            <Image size="30px" src={intercambio}/>
                        </Button>
                        <Button size='md' className="login-items">
                            <Image size="30px" src={notification}/>
                        </Button>
                        <IconButton aria-label="Preguntas frecuentes" icon="question-outline" variant="ghost" className="login-items"/>

                        <Menu>
                            <MenuButton as={Button} rightIcon="chevron-down" variant="ghost">
                            <Image
                                size="2rem"
                                rounded="full"
                                src={this.context.user.photoURL || require("../../GenericAssets/girl.png")} 
                                alt="Settings"
                            />
                            </MenuButton>
                            <MenuList color="black">
                                <MenuItem onClick={() => this.goToPage('/user/profile')}>
                                    <Box as={MdAccountCircle} marginRight="1rem"/> Mi Perfil
                                </MenuItem>
                                <MenuItem><Box as={MdRedeem} marginRight="1rem"/> Mis Ofertas de Intercambio</MenuItem>
                                <MenuItem><Box as={MdDone} marginRight="1rem"/> Intercambios Realizados</MenuItem>
                                <MenuItem><Box as={MdHelpOutline} marginRight="1rem"/> Preguntas Frecuentes</MenuItem>
                                <hr/>
                                <MenuItem onClick={this.logout}><Box as={MdExitToApp} marginRight="1rem"/> Cerrar sesión</MenuItem>
                            </MenuList>
                        </Menu>
                    </div>
                </nav>
            </div>
        )
    }
}

export default withRouter(Navbar)