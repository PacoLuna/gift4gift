import React, { useState } from 'react'
import {Input, InputGroup, InputRightElement, IconButton} from '@chakra-ui/core'
import {useHistory} from 'react-router-dom'

const Searchbar = (props) =>{
    const history = useHistory();
    const [search, setSearch] = useState("")

    const doSearch = (e) => {
        e.preventDefault();

        history.push({
            pathname: '/search',
            search: `?search=${search}`,
            state: {search}
        })
    }

    return (
        <div>
            <form onSubmit={doSearch}>
                <InputGroup size="md">
                    <Input 
                        placeholder="Buscar productos para intercambiar" 
                        value={search} 
                        onChange={e => setSearch(e.target.value)}
                        />
                    <InputRightElement width="4.5rem">
                        <IconButton aria-label="Search database" icon="search" size="md" h="1.75rem" onClick={doSearch}/>
                    </InputRightElement>
                </InputGroup>
            </form>
        </div>
    )
}

export default Searchbar;