import React, { useState } from 'react'
import {Heading,Image, Button, ButtonGroup} from '@chakra-ui/core';
import gift from '../../GenericAssets/giftbox.svg'
import '../../scss/TopBar.scss'
import { useHistory } from "react-router-dom";
 

export default function Topbar(props) {
  const [outline,setOutline] = useState(true)
  const history = useHistory();

  const goTo = (path) => {
    history.push(path);
  }

  return (
    <div>
      <nav className="main-navbar">
        <div className="logo-navbar">
          <Image size="40px" paddingRight="5%" src={gift}/>
          <Heading as="h2" size="xl">Gift4Gift</Heading>
        </div>

        <div className="buttons-navbar">
          <ButtonGroup spacing={4}>
            <Button 
              onClick={() => goTo("/login")} 
              variantColor="green" 
              size="md" 
              variant="solid"
            >
                Iniciar Sesión
            </Button>
            <Button 
              onClick={() => goTo("/signup")} 
              variantColor="green" 
              size="md" 
              variant={ outline? "outline" : "solid"} 
              onMouseEnter={()=>setOutline(false)}
              onMouseLeave={()=>setOutline(true)}
            >
              Regístrate
            </Button>
          </ButtonGroup>
        </div>
      </nav>
    </div>
  )
}