import React, { createContext, Component } from 'react'

const userContext = createContext({});
export default userContext

export class ContextProvider extends Component {
  state = {
    user: null,

    login: (userLogged) => {
      this.setState({ user: userLogged })
    }
  }

  render() {
    return (
      <userContext.Provider value={this.state}>
        {this.props.children}
      </userContext.Provider>
    )
  }
}
