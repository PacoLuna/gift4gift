import axios from 'axios';

export default  class CategoriesController {
    static all(){
        return axios.get(`${axios.defaults.baseURL}/categories`)
    }

    static show(id) {
        return axios.get(`${axios.defaults.baseURL}/categories/${id}`)
    }
}