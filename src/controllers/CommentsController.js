import axios from 'axios';

export default class CommentController{
    static create(id_gift, comment){
        return axios.post(`${axios.defaults.baseURL}/gifts/${id_gift}/comments/`, comment);
    }
}