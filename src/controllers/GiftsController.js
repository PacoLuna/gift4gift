import axios from 'axios'

export default class GiftController{
    static all(){
        return axios.get(`${axios.defaults.baseURL}/gifts`);
    }

    static create(gift){
        return axios.post(`${axios.defaults.baseURL}/gifts`, gift);
    }

    static create_offer(giftId, offerId) {
        return axios.post(`${axios.defaults.baseURL}/gifts/new_offer`, {gift_id: giftId, offer_id: offerId})
    }

    static find(id){
        return axios.get(`${axios.defaults.baseURL}/gifts/${id}`)
    }

    static search(search){
        return axios.get(`${axios.defaults.baseURL}/gifts`, { params: { search }})
    }
}
