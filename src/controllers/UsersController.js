import axios from 'axios';

export default class UsersController {

  static find(id) {
    return axios.get(`${axios.defaults.baseURL}/users/${id}`)
  }

  static create(userData) {
    return axios.post(`${axios.defaults.baseURL}/users/`, userData)
  }
}