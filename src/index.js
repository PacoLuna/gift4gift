import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import axios from 'axios'

import { ThemeProvider, CSSReset } from '@chakra-ui/core';
import {ContextProvider} from './context';

// firebase
import firebase from 'firebase/app';
import firebaseConfig from './config/firebaseConfig';
firebase.initializeApp(firebaseConfig);

axios.defaults.baseURL = 'https://gift4giftapi.herokuapp.com'

// chakra-ui

const Chakra = ({ children }) => (
  <ThemeProvider>
    <CSSReset />
    { children }
  </ThemeProvider>
);

ReactDOM.render(
  <ContextProvider>
    <Chakra>
      <App />
    </Chakra>
  </ContextProvider>, document.getElementById('root'));
  

serviceWorker.unregister();
