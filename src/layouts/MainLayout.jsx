import React, { useContext } from 'react'
import Topbar from '../components/Topbar/Topbar'
import Sidebar from '../components/LoginPage/Sidebar'
import './MainLayout.scss'
import userContext from '../context'
import Navbar from '../components/Topbar/Navbar'


const MainLayout = ({ children }) => {
  const context = useContext(userContext)
  
  return (
    <div className="main-layout">
      {context.user ? <Navbar /> : <Topbar />}
      <div className="container">
        <div className="left">
          <Sidebar />
        </div>
        <div className="right">
          {children}
        </div>
      </div>
    </div>
  )
}

export default MainLayout;