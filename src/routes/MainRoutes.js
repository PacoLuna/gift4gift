import React from 'react';
import UserProfile from '../view/Users/UserProfile';
import { BrowserRouter, Route, Switch } from "react-router-dom";

// Layout
import RouteLayout from './RouteLayout';

// Views
import HomeView from '../view/HomeView/HomeView'
import Dashboard from '../view/Dashboard/Dashboard'
import SignIn from '../components/SignIn';
import SignUp from '../view/Users/SignupView';
import NewGift from '../view/Gifts/NewGift'
import GiftsView from '../view/Gifts/GiftView'
import CategoryView from '../view/Categories/CategoryView';

const MainRoutes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={HomeView} /> 
      <Route exact path="/login" component={SignIn} />
      <Route exact path="/signup" component={SignUp} />
      <Route exact path="/login/:username" component={Dashboard}/>
      <RouteLayout exact path="/gifts/new" component={NewGift}/>
      <RouteLayout exact path="/gifts/:id" component={GiftsView}/>
      <RouteLayout exact path="/dashboard" component={Dashboard} />
      <RouteLayout exact path="/user/profile" component={UserProfile} />
      <RouteLayout exact path="/user/:id" component={UserProfile}/>
      <RouteLayout exact path="/userlogged/gifts/:id" component={GiftsView}/>
      <RouteLayout exact path="/categories/:id" component={CategoryView} />
      <RouteLayout exact path="/search" component={Dashboard} />
    </Switch>
  </BrowserRouter>
)

export default MainRoutes;
