import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import MainLayout from '../layouts/MainLayout';

import userContext from '../context';

const RouteLayout = ({ component: Component, ...rest }) => {
	const context = useContext(userContext);

	if(!context.user) 
		return <Redirect to="/" />

	return (
		<Route
			{...rest}
			render={(matchProps) => (
				<MainLayout>
					<Component />
				</MainLayout>
			)}
		/>
	);
};

export default RouteLayout;
