import React, { Component } from 'react';
import { Heading } from '@chakra-ui/core';
import { withRouter } from 'react-router-dom';
import CategoriesController from '../../controllers/CategoriesController';
import DashboardItem from '../../components/LoginPage/DashboardItem';

class CategoryView extends Component {

  state = {
    category: {}
  }

  componentDidMount() {
    const { id }= this.props.match.params;
    this.getCategory(id)
  }

  getCategory = (id) => {
    CategoriesController.show(id)
                        .then((res) => this.setState({ category: res.data }));
  }
  

  render() {
    const { category } = this.state;
    const { id }= this.props.match.params;

    if(id != category.id) this.getCategory(id)

    return (
      <div>
        <Heading style={{marginRight: "5rem", textAlign: "right"}}>{category.name}</Heading>
        <div class="gifts-list">
          {
          category.gifts && category.gifts.map((g) => !g.gift_id && (
            <DashboardItem key={g.id} gift={g} category={category.name}/>
            ))
          }
        </div>
      </div>
    )
  }
}

export default withRouter(CategoryView);