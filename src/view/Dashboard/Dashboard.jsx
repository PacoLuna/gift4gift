import React, { Component } from 'react'
import DashboardItem from '../../components/LoginPage/DashboardItem'
import GiftController from '../../controllers/GiftsController';
import { Heading } from '@chakra-ui/core'
import { withRouter } from 'react-router-dom';
import './Dashboard.scss'

class Dashboard extends Component {
  state = {
    gifts: [],
    last: null
  }

  componentDidMount() {
    this.getGifts()
  }

  getGifts = () => {
    const { state } = this.props.location
    if(state && state.search)
      GiftController.search(state.search)
                    .then((res) => this.setState({ gifts: res.data, last: state.search }))
    else
      GiftController.all().then((res) => this.setState({ gifts: res.data, last: "all" }))
  }

  renderGifts = () => {
    return this.state.gifts.map((g) => (
      <DashboardItem key={g.id} gift={g}/>
    ))
  }
  
  render() {
    const { state } = this.props.location;

    if(state && state.search != this.state.last) this.getGifts()

    return (
      <>
        <Heading style={{textAlign: 'center'}} marginBottom="1rem">
          Ofertas actuales { state && state.search ? `en ${state.search}` : ""}
        </Heading>
        <div className="gifts-list">
          { this.renderGifts() }
        </div>
      </>
    )
  }
}

export default withRouter(Dashboard);