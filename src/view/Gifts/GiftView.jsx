import React, { Component } from 'react'
import GiftsDetails from '../../components/GiftsDetails/GiftsDetails'
import CommentsBox from '../../components/CommentsBox/CommentsBox'
import {withRouter} from 'react-router-dom'
import GiftsController from '../../controllers/GiftsController';
import { Flex } from '@chakra-ui/core';
import OffersList from './OffersList';

class GiftView extends Component {  
  state = {
      gift: {}
  }
  componentDidMount() {
    const { id } = this.props.match.params;
    this.getGift(id) 
  }

  getGift = (id) => {

    GiftsController.find(id).then(data => this.setState({gift: data.data}))
  }

  componentWillReceiveProps(nextProps) {
    const { id } = nextProps.match.params;
    const {pathname} = this.props.location;
    const {pathname: nextPathname} = nextProps.location;
    
    if(pathname != nextPathname) this.getGift(id)
    else this.getGift(id)
  }

  render() {
    const {pathname} = this.props.location;
    return (
      <Flex flexDirection="column" size="100%">
        <GiftsDetails gift={this.state.gift}/>
        {
          pathname.startsWith("/userlogged/gifts/") 
          && <OffersList offers={this.state.gift.gifts || []} />
        }
        <CommentsBox comments={this.state.gift.comments || []} getGift={this.getGift} />
        </Flex>
    )
  }
}

export default withRouter(GiftView)
