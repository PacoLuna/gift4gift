import React, { Component } from 'react'
import GiftRegistrationForm from '../../components/NewGifts/GiftRegistrationForm'

export default class New extends Component {
    render() {
        return (
            <div>
                <GiftRegistrationForm/>
            </div>
        )
    }
}
