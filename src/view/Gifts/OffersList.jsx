import React from 'react';
import { Heading } from '@chakra-ui/core';
import DashboardItem from '../../components/LoginPage/DashboardItem';

const OffersList = ({ offers }) => (
	<div>
		<Heading size="md" marginTop="2rem">Ofertas para intercambiar</Heading>
    <div class="gifts-list">
      {offers.map((o, i) => <DashboardItem noPhoto key={i} gift={o} />)}
    </div>
	</div>
);

export default OffersList;
