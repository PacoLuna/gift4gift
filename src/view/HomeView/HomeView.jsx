import React, { Component } from 'react';
import Topbar from '../../components/Topbar/Topbar';
import Jumbotron from '../../components/Jumbotron/Jumbotron';
import { Redirect } from 'react-router-dom';

import userContext from '../../context';

class HomeView extends Component {
  static contextType = userContext;

	render() {
    const { user } = this.context;

    if(user) return <Redirect to="/dashboard"/>

		return (
			<div>
				<Topbar />
				<Jumbotron />
			</div>
		);
	}
}

export default HomeView;
