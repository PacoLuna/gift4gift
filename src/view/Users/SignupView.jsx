import React, { Component } from 'react'
import { Flex, Heading } from '@chakra-ui/core';

import Form from '../../components/SignUpForm';

export default class SignupView extends Component {
  render() {
    return (
      <Flex direction="column" justifyContent="center" alignItems="center">
        <Form bg={"white"}/>
      </Flex>
    )
  }
}
