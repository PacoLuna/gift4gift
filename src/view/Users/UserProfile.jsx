import React from 'react';
import {Stat, StatLabel, StatNumber, StatGroup, Flex, Box, Text, Avatar } from "@chakra-ui/core";
import photo from '../../GenericAssets/girl.png';
import { withRouter } from 'react-router-dom';
import UsersController from '../../controllers/UsersController';

import userContext from '../../context';
import DashboardItem from '../../components/LoginPage/DashboardItem';
import '../Dashboard/Dashboard';

class UserProfile extends React.Component{
  static contextType = userContext

  state = {
    id: "",
    full_name: "Adriana Campos Ortíz",
    email: "acampos@gmail.com",
    username: "",
    photo_url: photo,
    barter_rate: "16",
    score: "4.5",
    offers: "7"
  }

  componentDidMount() {
    this.getUser()
  }

  getUser = (reload) => {
    const { id } = this.props.match.params;

    if(id && !reload) UsersController.find(id).then((res) => this.setState({ ...res.data }))
    else UsersController.find(this.context.user.id).then((res) => this.setState({ ...res.data }))
    this.setState({
      score: Math.floor(Math.random() * 5),
    })
  }

  componentWillReceiveProps(nextProps) {
    const {pathname} = this.props.location;
    const {pathname: nextPathname} = nextProps.location;
    
    if(pathname != nextPathname) this.getUser(true)
    else this.getUser(false)
  }
  
  render(){
    const { pathname } = this.props.location;
    
    return(
      <>
        <Flex align="center" justify="center" display="flex" paddingX="10%" >
          <Flex align="center" justify="center" display="flex" w="30%" marginTop="5%">
            <Box>
              <Avatar size="2xl" src={this.state.photo_url || require("../../GenericAssets/girl.png")}/>
            </Box>
          </Flex>
          <Flex align="center" justify="center" display="flex-start" w="70%">
            <Box>
              <Text fontSize="2xl" fontWeight="medium" marginBottom="2%" marginTop ="5%">{this.state.full_name}</Text>
              <Text fontSize="lg" marginBottom="1%"> Contacto: {this.state.email} </Text>
              <StatGroup>
                <Stat>
                  <StatLabel>Intercambios</StatLabel>
                  <StatNumber>{this.state.gifts ? this.state.gifts.filter(g => !g.gift_id).length : 0}</StatNumber>              
                </Stat>
                <Stat>
                  <StatLabel>Puntuación</StatLabel>
                  <StatNumber>{this.state.score}</StatNumber>              
                </Stat>
                <Stat>
                  <StatLabel>ofertas</StatLabel>
                  <StatNumber>{this.state.gifts ? this.state.gifts.length : 0}</StatNumber>
                </Stat>
              </StatGroup>
            </Box>
          </Flex>
        </Flex>
        <div className="gifts-list" style={{marginTop: "2rem"}}>
          {
            this.state.gifts && this.state.gifts.map((offer) => (
              !offer.gift_id &&  <DashboardItem specifigPath={pathname.startsWith("/user/profile") ? "userlogged/gifts" : "gifts"} gift={offer} profile key={offer.id} />
            ))
          }
        </div>
      </>
    )
  }
}

export default withRouter(UserProfile);